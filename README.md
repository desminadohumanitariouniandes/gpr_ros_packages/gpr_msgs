# GPR-20 ROS Messages Package (gpr_msgs)

This package contains the messages that will be used in the GPR-20 robot ROS system. This package should be used in both the server and GPR workspaces. This package also contains two launch files, intended to also be used in the server and the GPR.